/**
 * @last modified on  : 10-27-2020
 * @last modified by  : anjali.rathore@mtxb2b.com
 * Modifications Log 
 * Ver   Date         Author                      Modification
 * 1.0   10-27-2020   anjali.rathore@mtxb2b.com   Initial Version
**/

public class CustomMetadataCallback implements Metadata.DeployCallback {
    public void handleResult(Metadata.DeployResult result,
                             Metadata.DeployCallbackContext context) {
        if (result.status == Metadata.DeployStatus.Succeeded) {
            System.debug('success: '+ result);
        } else {
            // Deployment was not successful
            System.debug('fail: '+ result);
        }
    }
}